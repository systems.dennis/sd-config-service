package net.orivis.shared.config_service.form;

import lombok.Data;
import net.orivis.shared.servers.model.ServerConfig;

@Data
public class ConfigServiceForm {
    private String property_name;
    private String property_value;
    private String value_class_type;
    private ServerConfig serverConfigValue;
    private String serverConfig;


    public String asValue() {
        return property_name;
    }
}
