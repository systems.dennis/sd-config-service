package net.orivis.shared.config_service.cron;

import lombok.extern.slf4j.Slf4j;
import net.orivis.apptodb.model.AppSettingsModel;
import net.orivis.apptodb.service.AppSettingsService;
import net.orivis.shared.config.OrivisContext;
import net.orivis.shared.utils.OrivisContextable;
import org.springframework.http.HttpMethod;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;
import net.orivis.shared.config_service.form.ConfigServiceForm;
import net.orivis.shared.servers.model.ServerConfig;
import net.orivis.shared.servers.model.ServerConfigType;
import net.orivis.shared.servers.service.ServerConfigService;
import net.orivis.shared.servers.service.ServerConfigTypeService;

@Slf4j
@Component
@EnableScheduling
public class RemoteConfigFetcher extends OrivisContextable {
    public RemoteConfigFetcher(OrivisContext context) {
        super(context);
    }

    @Scheduled (cron = "${global.app.config.remote_update_cron.schedule: 0 0/5 * * * ?}")
    public void UpdateSettings(){
        if (!getContext().getEnv("global.app.config.remote_update_cron.enabled", false)){
            return;
        }

        var path = getContext().getEnv("global.app.config.remote_update_cron.path", "http://localhost:3333");
        if (!path.endsWith("/")){
            path = path + "/";
        }
        var retrievePath = path +  "api/v2/configuration/fetch/scope/" + getContext().getEnv("app.config.remote_update_cron.scope", "sample_app");
        var appSettingsService = getBean(AppSettingsService.class);
        var serverService = getBean(ServerConfigService.class);

        try {
            var data = new RestTemplate().exchange(retrievePath, HttpMethod.GET, null,
                    ConfigServiceForm[].class).getBody();

            for (var item: data){
                if (item.getServerConfig() != null){

                    item.getServerConfigValue().getName();

                    var existing = serverService.findByServerConfigTypeName(item.getServerConfigValue().getServerConfigType().getName());
                    if (existing == null) {
                        var newConfigType  = new ServerConfigType();
                        newConfigType.setName(item.getServerConfigValue().getServerConfigType().getName());
                        getBean(ServerConfigTypeService.class).save(newConfigType);
                        item.getServerConfigValue().setServerConfigType(newConfigType);
                        item.setServerConfig(newConfigType.getId());
                    }


                    //here create or update existing server
                    createOrUpdateServer(item, serverService);
                } else {
                    //update or create entity!
                    createOrUpdateExistingEntry(item, appSettingsService);
                }



            }
        } catch (Exception e){
            log.error("cannot_invoke_retrieve_config_service", e);
        }

    }
    private void createOrUpdateServer(ConfigServiceForm form, ServerConfigService service){
        try {
            var server = form.getServerConfigValue();
            var existing = service.getRepository().filteredFirst(service.getFilterImpl().eq("name", server.getName())
                    .and(service.getFilterImpl().eq("type", server.getType()))).orElse(new ServerConfig());

            existing.setActive(server.getActive());
            existing.setHost(server.getHost());
            existing.setUserName(server.getUserName());
            existing.setPassword(server.getPassword());
            existing.setName(server.getName());
            existing.setServerConfigType(server.getServerConfigType());
            existing.setType(server.getType());
            existing.setTimeZone(server.getTimeZone());
            existing.setPort(server.getPort());
            existing.setServerParam(server.getServerParam());
            service.save(existing);
        } catch (Exception e){
            log.error("cannot_save_config_service_item", e);
        }
    }

    private void createOrUpdateExistingEntry(ConfigServiceForm item, AppSettingsService appSettingsService) {
        var existing =  appSettingsService.getRepository().filteredFirst(appSettingsService.getFilterImpl()
                .eq("key", item.getProperty_name())).orElse(new AppSettingsModel());
        existing.setKey(item.getProperty_name());
        existing.setValue(item.getProperty_value());
        existing.setValueClassType(item.getValue_class_type() == null ? "text": item.getValue_class_type());

        //! important when we recieve new setting or changed old one to update it in cache!
        getBean(AppSettingsService.class).addToCache(existing);
        try {
            appSettingsService.save(existing);
        } catch (Exception e){
            log.error("cannot_save_config_service_item", e);
        }
    }
}
