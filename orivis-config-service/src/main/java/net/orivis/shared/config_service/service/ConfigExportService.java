package net.orivis.shared.config_service.service;

import net.orivis.shared.config.OrivisContext;
import net.orivis.shared.utils.OrivisContextable;
import org.springframework.core.io.UrlResource;
import org.springframework.stereotype.Service;
import net.orivis.shared.config_service.exception.ExportException;
import net.orivis.shared.config_service.model.ConfigServiceModel;

import java.io.File;
import java.util.List;
import java.util.UUID;

import static net.orivis.shared.utils.Mapper.mapper;

@Service
public class ConfigExportService extends OrivisContextable {

    public ConfigExportService(OrivisContext context) {
        super(context);
    }

    public UrlResource exportToJson() {
        List<ConfigServiceModel> data = getBean(ConfigService.class).find();

        try {
            String filePath = getDownloadPath();

            File file = new File(filePath);
            mapper.writeValue(file, data);
            return new UrlResource(new File(filePath).toURI());
        } catch (Exception e) {
            throw new ExportException("error_exporting_data_to_json");
        }
    }

    private String getDownloadPath() {
        String directory = getContext().getEnv("global.download.path", "./");

        File dir = new File(directory);
        if (!dir.exists()) {
            dir.mkdirs();
        }

        return directory + UUID.randomUUID().toString() + ".json";
    }
}
