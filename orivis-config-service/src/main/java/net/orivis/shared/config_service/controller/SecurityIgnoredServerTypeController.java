package net.orivis.shared.config_service.controller;

import net.orivis.shared.annotations.OrivisController;
import net.orivis.shared.config.OrivisContext;
import net.orivis.shared.utils.OrivisContextable;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import net.orivis.shared.exceptions.ItemForAddContainsIdException;
import net.orivis.shared.postgres.controller.AddItemController;
import net.orivis.shared.postgres.controller.DeleteItemController;
import net.orivis.shared.postgres.controller.EditItemController;
import net.orivis.shared.postgres.controller.ListItemController;
import net.orivis.shared.servers.form.ServerConfigTypeForm;
import net.orivis.shared.servers.model.ServerConfigType;
import net.orivis.shared.servers.service.ServerConfigTypeService;

import java.io.Serializable;

@RestController
@RequestMapping("/api/v2/servers_types_ex")
@CrossOrigin
@OrivisController(ServerConfigTypeService.class)
public class SecurityIgnoredServerTypeController<TYPE_ID extends Serializable> extends OrivisContextable implements
        DeleteItemController<ServerConfigType>,
        AddItemController<ServerConfigType, ServerConfigTypeForm>,
        EditItemController<ServerConfigType, ServerConfigTypeForm>,
        ListItemController<ServerConfigType, ServerConfigTypeForm> {

    public SecurityIgnoredServerTypeController(OrivisContext context) {
        super(context );
    }

    @Override
    public ServerConfigTypeService getService() {
        return AddItemController.super.getService();
    }

    @Override
    public ResponseEntity<ServerConfigTypeForm> add(ServerConfigTypeForm form) throws ItemForAddContainsIdException {
        return AddItemController.super.add(form);
    }
}