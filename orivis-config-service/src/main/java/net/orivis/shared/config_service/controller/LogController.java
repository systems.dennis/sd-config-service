package net.orivis.shared.config_service.controller;

import net.orivis.shared.annotations.OrivisController;
import net.orivis.shared.config.OrivisContext;
import net.orivis.shared.utils.OrivisContextable;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import net.orivis.shared.annotations.security.Secured;
import net.orivis.shared.config_service.form.LogForm;
import net.orivis.shared.config_service.model.LogModel;
import net.orivis.shared.config_service.service.LogService;
import net.orivis.shared.postgres.controller.AddItemController;
import net.orivis.shared.postgres.controller.DeleteItemController;
import net.orivis.shared.postgres.controller.GetByIdController;
import net.orivis.shared.postgres.controller.ListItemController;

@RestController
@RequestMapping("/api/v2/log_center/log")
@OrivisController(value = LogService.class)
@Secured
@CrossOrigin
public class LogController extends OrivisContextable
        implements AddItemController<LogModel, LogForm>,
        GetByIdController<LogModel, LogForm>,
        ListItemController<LogModel, LogForm>,
        DeleteItemController<LogModel> {

    public LogController(OrivisContext context) {
        super(context);
    }
}
