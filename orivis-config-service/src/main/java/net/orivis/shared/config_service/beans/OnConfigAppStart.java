package net.orivis.shared.config_service.beans;

import net.orivis.shared.beans.OnOrivisStart;
import net.orivis.shared.config.OrivisContext;
import org.springframework.stereotype.Service;

@Service
public class OnConfigAppStart implements OnOrivisStart {
    @Override
    public void afterRun(OrivisContext context) {

    }
}
