package net.orivis.shared.config_service.repo;

import net.orivis.shared.config_service.model.ConfigServiceModel;
import net.orivis.shared.postgres.repository.OrivisRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ConfigServiceRepo extends OrivisRepository<ConfigServiceModel> {

}
