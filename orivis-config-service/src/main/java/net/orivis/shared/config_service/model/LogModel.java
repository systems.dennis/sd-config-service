package net.orivis.shared.config_service.model;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import lombok.Data;
import net.orivis.shared.annotations.FormTransient;
import net.orivis.shared.annotations.entity.Created;
import net.orivis.shared.postgres.model.LongAssignableEntity;
import net.orivis.shared.utils.bean_copier.DateToUTCConverter;
import net.orivis.shared.utils.bean_copier.OrivisTranformer;

import java.util.Date;

@Entity
@Data
public class LogModel extends LongAssignableEntity {

    @Created
    @FormTransient
    @OrivisTranformer(transFormWith  = DateToUTCConverter.class)
    private Date created;

    private String module;

    @Column(columnDefinition = "text")
    private String message;

    @Override
    public String asValue() {
        return module;
    }
}
