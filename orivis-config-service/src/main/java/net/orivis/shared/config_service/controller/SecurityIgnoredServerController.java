package net.orivis.shared.config_service.controller;

import net.orivis.shared.annotations.OrivisController;
import net.orivis.shared.config.OrivisContext;
import net.orivis.shared.utils.OrivisContextable;
import org.springframework.web.bind.annotation.*;
import net.orivis.shared.postgres.controller.AddItemController;
import net.orivis.shared.postgres.controller.DeleteItemController;
import net.orivis.shared.postgres.controller.EditItemController;
import net.orivis.shared.postgres.controller.ListItemController;
import net.orivis.shared.servers.form.ServerConfigForm;
import net.orivis.shared.servers.model.ServerConfig;
import net.orivis.shared.servers.service.ServerConfigService;

import java.io.Serializable;

@RestController
@RequestMapping("/api/v2/servers_ex")
@CrossOrigin
@OrivisController(ServerConfigService.class)
public class SecurityIgnoredServerController<TYPE_ID extends Serializable> extends OrivisContextable implements
        DeleteItemController<ServerConfig>,
        AddItemController<ServerConfig, ServerConfigForm>,
        EditItemController<ServerConfig, ServerConfigForm>,
        ListItemController<ServerConfig, ServerConfigForm> {

    public SecurityIgnoredServerController(OrivisContext context) {
        super(context );
    }

    @Override
    public ServerConfigService getService() {
        return AddItemController.super.getService();
    }

    @GetMapping("/type/{type}")
    public ServerConfigForm findByType(@PathVariable("type") Long type) {

        return toForm(getService().findByType(type, false));
    }
}