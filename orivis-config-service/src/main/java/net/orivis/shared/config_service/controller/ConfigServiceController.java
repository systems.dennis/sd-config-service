package net.orivis.shared.config_service.controller;

import net.orivis.shared.annotations.OrivisController;
import net.orivis.shared.config.OrivisContext;
import net.orivis.shared.controller.orivis.GeneratedReport;
import net.orivis.shared.utils.OrivisContextable;
import org.springframework.core.io.UrlResource;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import net.orivis.shared.config_service.form.ConfigServiceForm;
import net.orivis.shared.config_service.model.ConfigServiceModel;
import net.orivis.shared.config_service.service.ConfigExportService;
import net.orivis.shared.config_service.service.ConfigImportService;
import net.orivis.shared.config_service.service.ConfigService;
import net.orivis.shared.postgres.controller.AddItemController;
import net.orivis.shared.postgres.controller.DeleteItemController;
import net.orivis.shared.postgres.controller.GetByIdController;
import net.orivis.shared.postgres.controller.ListItemController;

import java.util.List;

@CrossOrigin
@RestController()
@RequestMapping("/api/v2/configuration")
@OrivisController(value = ConfigService.class)
public class ConfigServiceController extends OrivisContextable implements
        GetByIdController<ConfigServiceModel, ConfigServiceForm>,
        AddItemController<ConfigServiceModel, ConfigServiceForm>,
        DeleteItemController<ConfigServiceModel>,
        ListItemController<ConfigServiceModel, ConfigServiceForm>

{


    public ConfigServiceController(OrivisContext context) {
        super(context);
    }

    @GetMapping("/fetch/name/{name}/scope/{scope}")
    public ConfigServiceForm findByNameAndScope(@PathVariable("name") String name, @PathVariable ("scope") String scope){
        return  toForm(getService().byNameAndScope(name, scope));
    }
    @GetMapping("/fetch/scope/{scope}")
    public @ResponseBody List<ConfigServiceForm> findByScope(@PathVariable String scope){
        var res = getService().getSettings(scope);
        return res.stream().map(this::toForm).toList();
    }

    @Override
    public ConfigServiceForm afterTransformToForm(ConfigServiceForm ob, ConfigServiceModel origin) {
        return GetByIdController.super.afterTransformToForm(ob, origin);
    }

    @GetMapping("/apply_to_scope/{setting}/{scope}")
    public void applyToScope(@PathVariable("setting") Long setting, @PathVariable("scope") String scope){
        getService().applyToScope(scope, setting);
    }

    @PostMapping("/export")
    public GeneratedReport export() {
        UrlResource generatedReportPath = getBean(ConfigExportService.class).exportToJson();

        GeneratedReport report = new GeneratedReport();

        report.setPathToDownload(generatedReportPath.getFilename());
        report.setType("application/oset-stram");

        return report;
    }

    @PostMapping("/import")
    public void importXLS(@RequestParam("file") MultipartFile file) {
        ConfigImportService configImportService = getBean(ConfigImportService.class);
        configImportService.importFromJson(file);
    }

    @Override
    public ConfigService getService() {
        return AddItemController.super.getService();
    }

    @Override
    public ConfigServiceForm toForm(ConfigServiceModel model) {
        var res =  GetByIdController.super.toForm(model);
        res.setServerConfigValue(model.getServerConfig());
        return  res;
    }
}
