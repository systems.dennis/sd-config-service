package net.orivis.shared.config_service.service;

import net.orivis.shared.annotations.OrivisService;
import net.orivis.shared.config.OrivisContext;
import org.springframework.stereotype.Service;
import net.orivis.shared.annotations.DeleteStrategy;
import net.orivis.shared.config_service.form.LogForm;
import net.orivis.shared.config_service.model.LogModel;
import net.orivis.shared.config_service.repo.LogRepo;
import net.orivis.shared.postgres.service.PaginationService;

import static net.orivis.shared.annotations.DeleteStrategy.DELETE_STRATEGY_PROPERTY;

@Service
@OrivisService(model = LogModel.class, form = LogForm.class, repo = LogRepo.class)
@DeleteStrategy(value =  DELETE_STRATEGY_PROPERTY)
public class LogService extends PaginationService<LogModel> {

    public LogService(OrivisContext holder) {
        super(holder);
    }
}
