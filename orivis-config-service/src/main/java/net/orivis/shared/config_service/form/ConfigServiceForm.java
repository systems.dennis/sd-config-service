package net.orivis.shared.config_service.form;

import jakarta.persistence.Transient;
import lombok.Data;
import net.orivis.shared.annotations.ObjectByIdPresentation;
import net.orivis.shared.pojo_form.PojoFormElement;
import net.orivis.shared.pojo_view.UIAction;
import net.orivis.shared.pojo_view.list.PojoListView;
import net.orivis.shared.pojo_view.list.PojoListViewField;
import net.orivis.shared.pojo_view.list.Remote;
import net.orivis.shared.postgres.form.OrivisPojo;
import net.orivis.shared.scopes.controller.ScopeController;
import net.orivis.shared.scopes.service.ScopeService;
import net.orivis.shared.servers.controller.ServerController;
import net.orivis.shared.servers.model.ServerConfig;
import net.orivis.shared.servers.service.ServerConfigService;
import net.orivis.shared.utils.bean_copier.OrivisIdToObjectTransformer;
import net.orivis.shared.utils.bean_copier.OrivisTranformer;

import static net.orivis.shared.pojo_view.DEFAULT_TYPES.HIDDEN;
import static net.orivis.shared.pojo_view.DEFAULT_TYPES.OBJECT_SEARCH;

@PojoListView (actions = UIAction.ACTION_NEW)
@Data
public class ConfigServiceForm  implements OrivisPojo {
    @PojoFormElement(type = HIDDEN)
    private Long id;

    @PojoFormElement
    private String property_name;
    @PojoFormElement
    private String property_value;
    @PojoFormElement
    private String value_class_type;

    @Transient
    private ServerConfig serverConfigValue;

    @PojoFormElement(type = OBJECT_SEARCH, remote = @Remote(controller = ServerController.class))
    @PojoListViewField(type =  OBJECT_SEARCH, remote = @Remote(controller = ServerController.class))
    @OrivisTranformer(transFormWith = OrivisIdToObjectTransformer.class, additionalClass = ServerConfigService.class)
    private Long serverConfig;

    @ObjectByIdPresentation
    @PojoFormElement (type =  OBJECT_SEARCH, remote = @Remote(controller = ScopeController.class))
    @PojoListViewField(type =  OBJECT_SEARCH, remote = @Remote(controller = ScopeController.class))
    @OrivisTranformer(transFormWith = OrivisIdToObjectTransformer.class, additionalClass = ScopeService.class)
    private Long scope;


    @PojoListViewField(actions = {@UIAction(value = "copy_to_scope"),  @UIAction( "edit"),  @UIAction("delete")})
    private int actions;
    @Override
    public String asValue() {
        return property_name;
    }
}
