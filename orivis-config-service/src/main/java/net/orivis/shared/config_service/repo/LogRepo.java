package net.orivis.shared.config_service.repo;

import net.orivis.shared.config_service.model.LogModel;
import net.orivis.shared.postgres.repository.OrivisRepository;

public interface LogRepo extends OrivisRepository<LogModel> {
}
