package net.orivis.shared.config_service.service;


import net.orivis.shared.annotations.OrivisService;
import net.orivis.shared.config.OrivisContext;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import net.orivis.shared.annotations.DeleteStrategy;
import net.orivis.shared.config_service.model.ConfigServiceModel;
import net.orivis.shared.config_service.form.ConfigServiceForm;
import net.orivis.shared.config_service.repo.ConfigServiceRepo;
import net.orivis.shared.exceptions.ItemNotFoundException;
import net.orivis.shared.postgres.service.PaginationService;
import net.orivis.shared.scopes.model.ScopeModel;
import net.orivis.shared.scopes.service.ScopeService;
import net.orivis.shared.utils.bean_copier.BeanCopier;

import java.util.List;

@Service
@DeleteStrategy (DeleteStrategy.DELETE_STRATEGY_PROPERTY)
@OrivisService(model = ConfigServiceModel.class, form = ConfigServiceForm.class, repo = ConfigServiceRepo.class)
public class ConfigService  extends PaginationService<ConfigServiceModel> {
    public ConfigService(OrivisContext context) {
        super(context);
    }

    public ConfigServiceModel byNameAndScope(String name, String scopeName) {
        ScopeModel scopeElement = getBean(ScopeService.class).getRepository().filteredFirst(getFilterImpl().eq("name", scopeName)).orElseThrow(()-> ItemNotFoundException.fromId(scopeName));
        return  getRepository().filteredFirst(getFilterImpl().eq("scope", scopeElement).and(getFilterImpl().eq("name", name)))
                .orElseThrow(()->ItemNotFoundException.fromId(name));
    }

    public List<ConfigServiceModel> getSettings(String scopeName) {
        ScopeModel scopeElement = getBean(ScopeService.class).getRepository().filteredFirst(getFilterImpl().eq("name", scopeName)).orElseThrow(()-> ItemNotFoundException.fromId(scopeName));
        return filteredData(getFilterImpl().eq("scope", scopeElement), Pageable.unpaged()).getContent();
    }

    public void applyToScope(String scopeName, Long setting) {
        var scopeElement = getBean(ScopeService.class).getRepository().filteredFirst(getFilterImpl().eq("name", scopeName).and(getNotDeletedQuery())).orElseThrow(()-> ItemNotFoundException.fromId(scopeName));
        var settingElement = getRepository().findById(setting).orElseThrow(()-> ItemNotFoundException.fromId(setting));

        //todo check for existance!

        var settingsNew = getBean(BeanCopier.class).copy(settingElement, ConfigServiceModel.class );
        settingsNew.setId(null);
        settingElement.setScope(scopeElement);
        save(settingsNew);

    }
}
