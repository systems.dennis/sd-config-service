package net.orivis.shared.config_service.form;

import lombok.Data;
import net.orivis.shared.pojo_form.PojoFormElement;
import net.orivis.shared.pojo_view.list.PojoListView;
import net.orivis.shared.pojo_view.list.PojoListViewField;
import net.orivis.shared.pojo_view.list.Remote;
import net.orivis.shared.postgres.form.OrivisPojo;
import net.orivis.shared.utils.OrivisDate;
import net.orivis.shared.utils.bean_copier.DateToUTCConverter;
import net.orivis.shared.utils.bean_copier.OrivisTranformer;

import static net.orivis.shared.pojo_view.DEFAULT_TYPES.DATE;
import static net.orivis.shared.pojo_view.DEFAULT_TYPES.DATE_FORMAT_FULL;
import static net.orivis.shared.pojo_view.DEFAULT_TYPES.DATE_TIME;
import static net.orivis.shared.pojo_view.DEFAULT_TYPES.HIDDEN;

@Data
@PojoListView(actions = {"download", "settings"})
public class LogForm implements OrivisPojo {

    @PojoFormElement(type = HIDDEN)
    private Long id;

    @PojoFormElement(type = DATE, available = false)
    @PojoListViewField(searchable = true, type = DATE, format =  DATE_FORMAT_FULL,  remote = @Remote(searchType = DATE_TIME))
    @OrivisTranformer(transFormWith = DateToUTCConverter.class)
    private OrivisDate created;

    @PojoFormElement()
    @PojoListViewField(searchable = true)
    private String module;

    @PojoFormElement()
    @PojoListViewField(searchable = true)
    private String message;
}
