package net.orivis.shared.config_service.model;

import jakarta.persistence.Entity;
import jakarta.persistence.ManyToOne;
import lombok.Data;
import net.orivis.shared.annotations.ObjectByIdPresentation;
import net.orivis.shared.annotations.Validation;
import net.orivis.shared.postgres.model.OrivisEntity;
import net.orivis.shared.scopes.model.ScopeModel;
import net.orivis.shared.scopes.service.ScopeService;
import net.orivis.shared.servers.model.ServerConfig;
import net.orivis.shared.servers.service.ServerConfigService;
import net.orivis.shared.utils.bean_copier.OrivisIdToObjectTransformer;
import net.orivis.shared.utils.bean_copier.OrivisTranformer;
import net.orivis.shared.validation.ValueNotEmptyValidator;

@Entity @Data
public class ConfigServiceModel extends OrivisEntity {
    private String property_name;
    private String property_value;
    private String value_class_type;

    @ManyToOne
    @OrivisTranformer(transFormWith = OrivisIdToObjectTransformer.class, additionalClass = ServerConfigService.class)
    @ObjectByIdPresentation
    private ServerConfig serverConfig;

    @ObjectByIdPresentation
    @Validation (ValueNotEmptyValidator.class)
    @ManyToOne
    @OrivisTranformer(transFormWith = OrivisIdToObjectTransformer.class, additionalClass = ScopeService.class)
    private ScopeModel scope;
    @Override
    public String asValue() {
        return property_name;
    }
}
