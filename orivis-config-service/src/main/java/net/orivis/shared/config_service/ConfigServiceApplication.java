package net.orivis.shared.config_service;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.context.annotation.Bean;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.client.RestTemplate;
import net.orivis.shared.postgres.repository.PaginationRepositoryImpl;

@SpringBootApplication(scanBasePackages = {"net.orivis.shared.**"})

@EnableJpaRepositories(basePackages = {
		"net.orivis.shared.**"}, repositoryBaseClass = PaginationRepositoryImpl.class)
@EntityScan(basePackages = {
		"net.orivis.shared.**"
})
@CrossOrigin
@EnableScheduling
public class ConfigServiceApplication {

	public static void main(String[] args) {
		SpringApplication.run(ConfigServiceApplication.class, args);
	}


	@Bean
	public RestTemplate restTemplate() {
		return new RestTemplate();
	}
}
