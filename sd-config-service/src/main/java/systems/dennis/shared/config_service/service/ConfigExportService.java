package systems.dennis.shared.config_service.service;

import com.fasterxml.jackson.core.JsonProcessingException;
import org.springframework.stereotype.Service;
import systems.dennis.shared.config.WebContext;
import systems.dennis.shared.config_service.exception.ExportException;
import systems.dennis.shared.config_service.model.ConfigServiceModel;
import systems.dennis.shared.entity.KeyValue;
import systems.dennis.shared.servers.model.ServerConfig;
import systems.dennis.shared.utils.ApplicationContext;
import systems.dennis.shared.utils.bean_copier.BeanCopier;

import java.io.File;
import java.util.*;

import static systems.dennis.shared.utils.Mapper.mapper;

@Service
public class ConfigExportService extends ApplicationContext {


    private final BeanCopier beanCopier;

    public ConfigExportService(WebContext context, BeanCopier beanCopier) {
        super(context);
        this.beanCopier = beanCopier;
    }

    public String exportToJson(List<KeyValue> replacements) {
        List<ConfigServiceModel> data = getBean(ConfigService.class).find();

        Map<String, Object> replacementsMap = new HashMap<>();
        for (var replacement : replacements){
            replacementsMap.put(replacement.getKey(), replacement.getValue());
        }

        List<ConfigServiceModel> result = new ArrayList<>();

        for(ConfigServiceModel model: data){
            if (replacementsMap.get(model.getProperty_name()) != null){
                result.add(updateModel(model, replacementsMap.get(model.getProperty_name())));
            } else {
                result.add(model);
            }
        }
        try {
            return mapper.writeValueAsString(data);
        } catch (Exception e) {
            throw new ExportException("error_exporting_data_to_json");
        }
    }

    private ConfigServiceModel updateModel(ConfigServiceModel model, Object o) {

        var result = beanCopier.clone(model);
        try {
            result.setServerConfig(mapper.readValue(String.valueOf(o), ServerConfig.class));
        } catch (JsonProcessingException e) {
            //cannot set server config, then leave old one
            result.setServerConfig( model.getServerConfig());
        }
        return result;
    }
}
