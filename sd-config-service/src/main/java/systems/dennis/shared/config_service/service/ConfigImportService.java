package systems.dennis.shared.config_service.service;

import com.fasterxml.jackson.core.type.TypeReference;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;
import systems.dennis.shared.config.WebContext;
import systems.dennis.shared.config_service.exception.ImportException;
import systems.dennis.shared.config_service.model.ConfigServiceModel;
import systems.dennis.shared.repository.AbstractDataFilter;
import systems.dennis.shared.scopes.model.ScopeModel;
import systems.dennis.shared.scopes.service.ScopeService;
import systems.dennis.shared.servers.model.ServerConfig;
import systems.dennis.shared.servers.model.ServerConfigType;
import systems.dennis.shared.servers.service.ServerConfigService;
import systems.dennis.shared.servers.service.ServerConfigTypeService;
import systems.dennis.shared.utils.ApplicationContext;

import java.util.List;
import java.util.Objects;
import java.util.Optional;

import static systems.dennis.shared.utils.Mapper.mapper;

@Service
public class ConfigImportService extends ApplicationContext {

    public ConfigImportService(WebContext context) {
        super(context);
    }

    public void importFromJson(MultipartFile file) {
        try {
            List<ConfigServiceModel> configs = mapper.readValue(
                    file.getInputStream(), new TypeReference<List<ConfigServiceModel>>() {});

            for (ConfigServiceModel config : configs) {
                ScopeModel scope = processScopeModel(config.getScope());
                ServerConfig serverConfig = processServerConfig(config.getServerConfig());

                ConfigService configService = getBean(ConfigService.class);

                config.setId(null);
                config.setScope(scope);
                config.setServerConfig(serverConfig);

                AbstractDataFilter<ConfigServiceModel> filter = configService.getFilterImpl().eq("scope", config.getScope());

                if (config.getProperty_name() != null) {
                    filter.and(configService.getFilterImpl().eq("property_name", config.getProperty_name()));
                }
                if (config.getServerConfig() != null) {
                    filter.and(configService.getFilterImpl().eq("serverConfig", config.getServerConfig()));
                }

                Optional<ConfigServiceModel> existingConfig = configService.filteredFirst(filter);

                if (existingConfig.isEmpty()) {
                    configService.save(config);
                }
            }
        } catch (Exception e) {
            throw new ImportException("error_import_data_from_json");
        }
    }

    private ScopeModel processScopeModel(ScopeModel scope) {
        if (Objects.isNull(scope)) {
            return null;
        }
        ScopeService scopeService = getBean(ScopeService.class);

        Optional<ScopeModel> existing = scopeService.filteredFirst(scopeService.getFilterImpl().eq("name", scope.getName()));

        scope.setId(null);
        return existing.orElseGet(() -> scopeService.save(scope));
    }

    private ServerConfig processServerConfig(ServerConfig serverConfig) {
        if (Objects.isNull(serverConfig)) {
            return null;
        }

        ServerConfigService serverConfigService = getBean(ServerConfigService.class);
        ServerConfigType serverConfigType = null;

        if (Objects.nonNull(serverConfig.getServerConfigType())) {
            serverConfigType = processServerConfigType(serverConfig.getServerConfigType());
        }

        AbstractDataFilter<ServerConfig> filter = serverConfigService.getFilterImpl().eq("host", serverConfig.getHost())
                .and(serverConfigService.getFilterImpl().eq("name", serverConfig.getName()))
                .and(serverConfigService.getFilterImpl().eq("port", serverConfig.getPort()));

        if (Objects.nonNull(serverConfigType)) {
            filter.and(serverConfigService.getFilterImpl().eq("serverConfigType", serverConfigType));
        }

        if (Objects.nonNull(serverConfig.getType())) {
            filter.and(serverConfigService.getFilterImpl().eq("type", serverConfig.getType()));
        }

        Optional<ServerConfig> existing = serverConfigService.filteredFirst(filter);

        serverConfig.setId(null);
        return existing.orElseGet(() -> serverConfigService.save(serverConfig));
    }

    private ServerConfigType processServerConfigType(ServerConfigType serverConfigType) {
        if (Objects.isNull(serverConfigType)) {
            return null;
        }

        ServerConfigTypeService serverConfigTypeService = getBean(ServerConfigTypeService.class);
        Optional<ServerConfigType> existing = serverConfigTypeService.filteredFirst(serverConfigTypeService.getFilterImpl().eq("name", serverConfigType.getName()));

        serverConfigType.setId(null);
        return existing.orElseGet(() -> serverConfigTypeService.save(serverConfigType));
    }
}
