package systems.dennis.shared.config_service.controller;

import org.springframework.core.io.UrlResource;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import systems.dennis.shared.annotations.WebFormsSupport;
import systems.dennis.shared.annotations.security.WithRole;
import systems.dennis.shared.config.WebContext;
import systems.dennis.shared.config_service.form.ConfigServiceForm;
import systems.dennis.shared.config_service.model.ConfigServiceModel;
import systems.dennis.shared.config_service.service.ConfigExportService;
import systems.dennis.shared.config_service.service.ConfigImportService;
import systems.dennis.shared.config_service.service.ConfigService;
import systems.dennis.shared.controller.items.magic.GeneratedReport;
import systems.dennis.shared.entity.KeyValue;
import systems.dennis.shared.postgres.controller.AddItemController;
import systems.dennis.shared.postgres.controller.DeleteItemController;
import systems.dennis.shared.postgres.controller.GetByIdController;
import systems.dennis.shared.postgres.controller.ListItemController;
import systems.dennis.shared.utils.ApplicationContext;

import java.util.List;

@CrossOrigin
@RestController()
@RequestMapping("/api/v2/configuration")
@WebFormsSupport(value = ConfigService.class)
public class ConfigServiceController extends ApplicationContext implements
        GetByIdController<ConfigServiceModel, ConfigServiceForm>,
        AddItemController<ConfigServiceModel, ConfigServiceForm>,
        DeleteItemController<ConfigServiceModel>,
        ListItemController<ConfigServiceModel, ConfigServiceForm>

{


    public ConfigServiceController(WebContext context) {
        super(context);
    }

    @GetMapping("/fetch/name/{name}/scope/{scope}")
    public ConfigServiceForm findByNameAndScope(@PathVariable("name") String name, @PathVariable ("scope") String scope){
        return  toForm(getService().byNameAndScope(name, scope));
    }
    @GetMapping("/fetch/scope/{scope}")
    public @ResponseBody List<ConfigServiceForm> findByScope(@PathVariable String scope){
        var res = getService().getSettings(scope);
        return res.stream().map(this::toForm).toList();
    }

    @Override
    public ConfigServiceForm afterTransformToForm(ConfigServiceForm ob, ConfigServiceModel origin) {
        return GetByIdController.super.afterTransformToForm(ob, origin);
    }

    @GetMapping("/apply_to_scope/{setting}/{scope}")
    public void applyToScope(@PathVariable("setting") Long setting, @PathVariable("scope") String scope){
        getService().applyToScope(scope, setting);
    }

    @PostMapping(value = "/export", consumes = "application/json", produces = "application/json")
    public String export(@RequestBody (required = false) List<KeyValue> replacements) {
        return getBean(ConfigExportService.class).exportToJson(replacements);
    }

    @PostMapping(value = "/import", consumes =  "application/json")
    public void importFromJson(@RequestParam("file") MultipartFile file) {
        ConfigImportService configImportService = getBean(ConfigImportService.class);
        configImportService.importFromJson(file);
    }

    @Override
    public ConfigService getService() {
        return AddItemController.super.getService();
    }

    @Override
    public ConfigServiceForm toForm(ConfigServiceModel model) {
        var res =  GetByIdController.super.toForm(model);
        res.setServerConfigValue(model.getServerConfig());
        return  res;
    }
}
