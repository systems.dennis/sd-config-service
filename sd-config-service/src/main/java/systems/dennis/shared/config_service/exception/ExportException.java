package systems.dennis.shared.config_service.exception;

import systems.dennis.shared.exceptions.StandardException;

import java.io.Serializable;

public class ExportException extends StandardException {

    public ExportException(Serializable target, String message) {
        super(target, message);
    }

    public ExportException(String message) {
        super(null, message);
    }
}
