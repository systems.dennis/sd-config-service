package systems.dennis.shared.config_service.exception;

import systems.dennis.shared.exceptions.StandardException;

import java.io.Serializable;

public class ImportException extends StandardException {

    public ImportException(Serializable target, String message) {
        super(target, message);
    }

    public ImportException(String message) {
        super(null, message);
    }
}
